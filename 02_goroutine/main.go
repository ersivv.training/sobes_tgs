package main

import (
	"fmt"
	"sync"
)

func main() {
	wg := &sync.WaitGroup{}
	m := make(chan string, 3)
	ff := &sync.Mutex{}

	for i := 0; i < 5; i++ {

		wg.Add(1)
		go func(i int, mm chan string, fff *sync.Mutex) {
			defer wg.Done()
			fff.Lock()
			mm <- fmt.Sprintf("Goroutine %d", i)
			fff.Unlock()
		}(i, m, ff)
	}

	for {
		select {
		case q := <-m:
			fmt.Println(q)
		}
	}
}
