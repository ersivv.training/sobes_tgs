package main

import "fmt"

func main() {
	var numbers []*int
	for _, v := range []int{1, 2, 3, 4} {
		numbers = append(numbers, &v)
	}

	double(numbers)

	for _, v := range numbers {
		fmt.Printf("%d", *v)
	}

}

func double(numbers []*int) {}
