package main

import "fmt"

type My interface {
	Get() string
}

type P struct {
	f string
}

func (p *P) Get() string {
	return p.f
}

func Execute(i My) {
	fmt.Println(i.Get())
}

func main() {
	hel := P{"hello"}
	//hel := &P{"hello"}
	Execute(hel)
}
